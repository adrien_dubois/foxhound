/**
 * @description Get all the photos of the public folder that have a number for name
 */
export const clients = [...new Array(10)].map((client, index) => ({
    href: `/${index + 1}.png`,
}));