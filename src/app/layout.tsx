import type { Metadata } from "next";
import { DM_Sans } from "next/font/google";
import "./globals.css";
import { ThemeProvider } from "@/providers/theme-provider";

const font = DM_Sans({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "FoxHound.",
  description: "Automatisez votre travail avec FoxHound.",
  icons: {
    icon:[
      {
        media: '(prefers-color-scheme: light)',
        url:'/fox.png',
        href:'/fox.png'
      },
      {
        media: '(prefers-color-scheme: dark)',
        url:'/fox.png',
        href:'/fox.png'
      },
    ]
  }
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={font.className}>
      <ThemeProvider
          attribute="class"
          defaultTheme="dark"
          enableSystem
        >
          {children}
        </ThemeProvider>
      </body>
    </html>
  );
}
